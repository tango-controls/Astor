//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// $Author$
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision$
//
//-======================================================================


package admin.astor;

import admin.astor.dev_state_viewer.ServerStatesDialog;
import admin.astor.tools.DeviceHierarchyDialog;
import admin.astor.tools.PollingProfiler;
import admin.astor.tools.PopupTable;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DbServInfo;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *	This class display a JPopupMenu.for server commands
 *
 * @author verdier
 */
public class ServerPopupMenu extends JPopupMenu implements AstorDefs {

    private final TangoHost host;
    private TangoServer [] servers;
    private LevelTree tree;
    private final JFrame jFrame;
    private final HostInfoDialog parent;
    private final int mode;

    static final int SERVER = 0;
    static final int LEVELS = 1;
    static final int SERVERS = 2;

    private static final String[] SERVER_MENU_LABELS = {
            "Start server",
            "Restart server",
            "Set startup level",
            "Up time",
            "Polling Manager",
            "Polling Profiler",
            "Pool Threads Manager",
            "Configure With Jive",
            "Configure With Wizard",
            "DB Attribute Properties",
            "Server Info",
            "Class  Info",
            "Device Dependencies",
            "Test   Device",
            "Check  States",
            "Black  Box",
            "Export Server to another TANGO_HOST",
            "Starter logs",
            "Standard Error",
    };
    private static final String[] LEVEL_MENU_LABELS = {
            "Start servers",
            "Stop  servers",
            "Change level number",
            "Up time",
            "Expand Tree",
    };
    static private final int OFFSET = 2;        //	Label And separator
    static private final int START_STOP = 0;
    static private final int RESTART = 1;
    static private final int STARTUP_LEVEL = 2;
    static private final int UPTIME = 3;
    static private final int POLLING_MANAGER = 4;
    static private final int POLLING_PROFILER = 5;
    static private final int POOL_THREAD_MAN = 6;
    static private final int CONFIGURE_JIVE   = 7;
    static private final int CONFIGURE_WIZARD = 8;
    static private final int DB_ATTRIBUTES = 9;
    static private final int SERVER_INFO = 10;
    static private final int CLASS_INFO = 11;
    static private final int DEPENDENCIES = 12;
    static private final int TEST_DEVICE = 13;
    static private final int CHECK_STATES = 14;
    static private final int BLACK_BOX = 15;
    static private final int EXPORT_SERVER = 16;
    static private final int STARTER_LOGS = 17;
    static private final int STD_ERROR = 18;

    static private final int START = 0;
    static private final int STOP = 1;
    static private final int CHANGE_LEVEL = 2;
    static private final int UPTIME_LEVEL = 3;
    static private final int EXPAND = 4;

    static private final boolean TANGO_7 = true;

    //==========================================================
    //==========================================================
    public ServerPopupMenu(JFrame jFrame, HostInfoDialog parent, TangoHost host, int mode) {
        super();
        this.jFrame = jFrame;
        this.parent = parent;
        this.host = host;
        this.mode = mode;

        buildBtnPopupMenu();
    }
    //===============================================================

    /**
     * Create a Popup menu for host control
     */
    //===============================================================
    private void buildBtnPopupMenu() {
        JLabel title = new JLabel("Server Control :");
        title.setFont(new java.awt.Font("Dialog", Font.BOLD, 16));
        add(title);
        add(new JPopupMenu.Separator());
        String[] pMenuLabels;
        switch (mode) {
            case SERVER:
                pMenuLabels = SERVER_MENU_LABELS;
                break;
            case LEVELS:
                pMenuLabels = LEVEL_MENU_LABELS;
                break;
            case SERVERS:
                pMenuLabels = LEVEL_MENU_LABELS;
                break;
            default:
                return;
        }

        for (String lbl : pMenuLabels) {
            JMenuItem btn = new JMenuItem(lbl);
            btn.addActionListener(this::cmdActionPerformed);
            add(btn);
        }
    }

    //======================================================
    //======================================================
    public void showMenu(MouseEvent evt, JTree tree, TangoServer server) {
        //	Set selection at mouse position
        TreePath selectedPath =
                tree.getPathForLocation(evt.getX(), evt.getY());

        if (selectedPath == null)
            return;
        tree.setSelectionPath(selectedPath);

        String name = server.getName();

        //	Add host name in menu label title
        JLabel lbl = (JLabel) getComponent(0);
        lbl.setText("  " + name + "  :");

        this.servers = new TangoServer[]{server};

        //	Set label (depends on server runninig or not)
        JMenuItem mi = (JMenuItem) getComponent(START_STOP + OFFSET);
        boolean running = (server.getState() == DevState.ON);

        if (running || server.getState() == DevState.MOVING)
            mi.setText("Kill  Server");
        else
            mi.setText("Start Server");

        //	And set if enable or not
        getComponent(RESTART + OFFSET).setEnabled(running);
        getComponent(POLLING_PROFILER + OFFSET).setEnabled(running);
        getComponent(TEST_DEVICE + OFFSET).setEnabled(running);
        getComponent(CHECK_STATES + OFFSET).setEnabled(running);
        getComponent(BLACK_BOX + OFFSET).setEnabled(running);
        getComponent(EXPORT_SERVER + OFFSET).setEnabled(true);
        getComponent(CONFIGURE_WIZARD + OFFSET).setEnabled(running);
        getComponent(DB_ATTRIBUTES + OFFSET).setVisible(!running);


        getComponent(POOL_THREAD_MAN + OFFSET).setVisible(TANGO_7);
        getComponent(DEPENDENCIES + OFFSET).setVisible(TANGO_7);

        getComponent(CONFIGURE_JIVE + OFFSET).setEnabled(true);

        //  Manage for READ_ONLY mode
        if (Astor.rwMode!=AstorDefs.READ_WRITE) {
            getComponent(OFFSET + EXPORT_SERVER).setVisible(false);
        }
        if (Astor.rwMode==AstorDefs.READ_ONLY) {
            getComponent(OFFSET + START_STOP).setVisible(false);
            getComponent(OFFSET + RESTART).setVisible(false);
            getComponent(OFFSET + STARTUP_LEVEL).setVisible(false);
            getComponent(OFFSET + POLLING_MANAGER).setVisible(false);
            getComponent(OFFSET + POOL_THREAD_MAN).setVisible(false);
            getComponent(OFFSET + TEST_DEVICE).setVisible(false);
        }
        if (Astor.rwMode!=AstorDefs.READ_WRITE) {
            getComponent(OFFSET + CONFIGURE_WIZARD).setVisible(false);
        }
        //	Get position and display Popup menu
        location = tree.getLocationOnScreen();
        location.x += evt.getX();
        location.y += evt.getY();
        show(tree, evt.getX(), evt.getY());
    }

    
    //======================================================
    /*
      *	Multiple Server Selection Clicked mouse
      */
    //======================================================
    public void showMenu(MouseEvent evt, LevelTree tree, TreePath [] selection) {
        this.tree = tree;
        this.servers = new TangoServer[selection.length];
        for(int i = 0 ; i < this.servers.length ; i++){
            this.servers[i] = (TangoServer)((DefaultMutableTreeNode)selection[i].getLastPathComponent()).getUserObject();
        }
        
        //	Add host name in menu label title
        JLabel lbl = (JLabel) getComponent(0);
        lbl.setText("  " + selection.length + " Servers Selected :");
        
        JMenuItem mi = (JMenuItem) getComponent(EXPAND + OFFSET);
        mi.setVisible(false);

        //  Manage for READ_ONLY mode
        if (Astor.rwMode==AstorDefs.READ_ONLY) {
            getComponent(OFFSET + START).setVisible(false);
            getComponent(OFFSET + STOP).setVisible(false);
            getComponent(OFFSET + CHANGE_LEVEL).setVisible(false);
        }

        //	Get position and display Popup menu
        location = tree.getLocationOnScreen();
        location.x += evt.getX();
        location.y += evt.getY();
        show(tree, evt.getX(), evt.getY());
    }
    
    //======================================================
    /*
      *	Manage event on clicked mouse on Level object.
      */
    //======================================================
    public void showMenu(MouseEvent evt, LevelTree tree, boolean expanded) {
        this.tree = tree;
        
        this.servers = this.tree.getTangoServerList().toArray(new TangoServer[0]);

        //	Add host name in menu label title
        JLabel lbl = (JLabel) getComponent(0);
        lbl.setText("  " + tree + "  :");
        
        JMenuItem mi = (JMenuItem) getComponent(EXPAND + OFFSET);
        mi.setVisible(true);
        mi.setText((expanded) ? "Collapse Tree" : LEVEL_MENU_LABELS[EXPAND]);

        //  Manage for READ_ONLY mode
        if (Astor.rwMode==AstorDefs.READ_ONLY) {
            getComponent(OFFSET + START).setVisible(false);
            getComponent(OFFSET + STOP).setVisible(false);
            getComponent(OFFSET + CHANGE_LEVEL).setVisible(false);
        }

        //	Get position and display Popup menu
        location = tree.getLocationOnScreen();
        location.x += evt.getX();
        location.y += evt.getY();
        show(tree, evt.getX(), evt.getY());
    }
    //======================================================
    /**
     * Called when popup menu item selected
     */
    //======================================================
    private Point location;
    private void cmdActionPerformed(ActionEvent evt) {
        switch (mode) {
            case SERVER:
                serverCmdActionPerformed(evt);
                break;
            case LEVELS:
                levelCmdActionPerformed(evt);
                break;
            case SERVERS:
                serversCmdActionPerformed(evt);
                break;
        }
    }
    //======================================================
    //======================================================
    private void levelCmdActionPerformed(ActionEvent evt) {
        Object obj = evt.getSource();
        int itemIndex = -1;
        for (int i = 0; i < LEVEL_MENU_LABELS.length; i++)
            if (getComponent(OFFSET + i) == obj)
                itemIndex = i;

        switch (itemIndex) {
            case START:
                parent.startLevel(tree.getLevelRow());
                break;
            case STOP:
                parent.stopLevel(tree.getLevelRow());
                break;
            case CHANGE_LEVEL:
                tree.changeChangeLevel(tree.getLevelRow());
                break;
            case UPTIME_LEVEL:
                tree.displayUptime();
                break;
            case EXPAND:
                tree.toggleExpandCollapse();
                break;
        }
    }
    //======================================================
    //======================================================
    private void serversCmdActionPerformed(ActionEvent evt) {
        Object obj = evt.getSource();
        int itemIndex = -1;
        for (int i = 0; i < LEVEL_MENU_LABELS.length; i++)
            if (getComponent(OFFSET + i) == obj)
                itemIndex = i;

        switch (itemIndex) {
            case START:
                new ServerCmdThread((JDialog)parent, host, START_ALL_SERVER, servers).start();
                break;
            case STOP:
                new ServerCmdThread((JDialog)parent, host, STOP_ALL_SERVER, servers).start();
                break;
            case CHANGE_LEVEL:
                //something to do here
                try{
                    PutServerInfoDialog dialog = new PutServerInfoDialog(parent, true);
                    dialog.setLocation(location);
                    servers[0].updateStartupInfo();
                    String serversList = AstorUtil.buildServerList(servers);
                    if(dialog.showDialog(servers[0].info,"<html>Change level for " + servers.length + " Servers:<br />" + serversList + "</html>") == PutServerInfoDialog.RET_OK){
                        DbServInfo info = dialog.getSelection();
                        for(TangoServer server : servers){
                            try{
                                info.name = server.getName();
                                server.putStartupInfo(info);
                                server.startupLevel(parent, host.getName(), location,true);   
                            }catch (DevFailed ex){
                                System.err.println("Failed to: " + ex.errors[0].desc);
                            }
                        }
                        parent.updateData();
                    }
                }catch (DevFailed ex){
                    System.err.println("Failed to: " + ex.errors[0].desc);
                }
                break;
            case UPTIME_LEVEL:
                displayUptime(servers);
                break;
        }
    }
    //======================================================
    //======================================================
    void displayUptime(TangoServer[] servers) {
        java.util.List<String[]> v = new ArrayList<>();
        try {
            for(TangoServer server:servers){
                String[] exportedStr = server.getServerUptime();
                v.add(new String[]{
                        server.getName(), exportedStr[0], exportedStr[1]});
            }
            String[] columns = new String[]{"Server", "Last   exported", "Last unexported"};
            String[][] table = new String[v.size()][];
            for (int i = 0; i < v.size(); i++)
                table[i] = v.get(i);
            PopupTable ppt = new PopupTable(parent, "Uptime from Selection " + servers.length + " Servers",
                    columns, table, new Dimension(650, 250));
            ppt.setVisible(true);
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(parent, null, e);
        }
    }
    //======================================================
    //======================================================
    private void serverCmdActionPerformed(ActionEvent evt) {
        Object obj = evt.getSource();
        int index = -1;
        for (int i = 0; i < SERVER_MENU_LABELS.length; i++)
            if (getComponent(OFFSET + i) == obj)
                index = i;

        switch (index) {
            case STARTUP_LEVEL:
                boolean changed = false;
                for(TangoServer server : servers){
                    if (server.startupLevel(parent, host.getName(), location))
                        changed = true;
                }
                if(changed)
                    parent.updateData();
                break;
            case UPTIME:
                try {
                    String[] exportedStr = servers[0].getServerUptime();
                    String[] columns = new String[]{"Last   exported", "Last unexported"};
                    PopupTable ppt = new PopupTable(parent, servers[0].getName(),
                            columns, new String[][]{exportedStr}, new Dimension(450, 50));
                    ppt.setVisible(true);
                } catch (DevFailed e) {
                    ErrorPane.showErrorMessage(parent, null, e);
                }
                break;
            case POLLING_MANAGER:
                if (servers[0].getState() == DevState.ON)
                    new ManagePollingDialog(parent, servers[0]).setVisible(true);
                else
                    try {
                        new DbPollPanel(parent, servers[0].getName()).setVisible(true);
                    } catch (DevFailed e) {
                        ErrorPane.showErrorMessage(parent, null, e);
                    }
                break;
            case POOL_THREAD_MAN:
                servers[0].poolThreadManager(parent, host);
                break;
            case POLLING_PROFILER:
                startPollingProfiler();
                break;
            case TEST_DEVICE:
                servers[0].testDevice(parent);
                break;
            case CHECK_STATES:
                //server.checkStates(parent);
                try {
                    new ServerStatesDialog(parent, servers[0].getName()).setVisible(true);
                } catch (DevFailed e) {
                    ErrorPane.showErrorMessage(parent, servers[0].getName(), e);
                }
                break;
            case BLACK_BOX:
                servers[0].displayBlackBox(parent);
                break;
            case STARTER_LOGS:
                host.displayLogging(parent, servers[0].getName());
                break;
            case CONFIGURE_JIVE:
                servers[0].showJive(jFrame);
                break;
            case CONFIGURE_WIZARD:
                servers[0].configureWithWizard(parent);
                break;
            case SERVER_INFO:
                servers[0].displayServerInfo(parent);
                break;
            case DB_ATTRIBUTES:
                servers[0].manageMemorizedAttributes(parent);
                break;
            case CLASS_INFO:
                servers[0].displayClassInfo(jFrame);
                break;
            case DEPENDENCIES:
                try {
                    new DeviceHierarchyDialog(parent,
                            servers[0].getName()).setVisible(true);
                } catch (DevFailed e) {
                    ErrorPane.showErrorMessage(parent, null, e);
                }
                break;
            case STD_ERROR:
                if (servers != null)
                    host.readStdErrorFile(parent, servers[0].getName());
                else
                    host.readStdErrorFile(parent, notifyd_prg + "/" + host.getName());
                break;
            case EXPORT_SERVER:
                try {
                    new admin.astor.tools.Server2TangoHost(parent, servers[0].get_server_name()).setVisible(true);
                    parent.updateData();
                } catch (DevFailed e) {
                    ErrorPane.showErrorMessage(parent, null, e);
                }
                break;
            case RESTART:
                servers[0].restart(parent, host, true);
                break;

            case START_STOP:
                if (servers[0].getState() == DevState.ON ||
                        servers[0].getState() == DevState.MOVING) {
                    try {
                        //	Ask to confirm
                        if (JOptionPane.showConfirmDialog(parent,
                                "Are you sure to want to kill " + servers[0].getName(),
                                "Confirm Dialog",
                                JOptionPane.YES_NO_OPTION) != JOptionPane.OK_OPTION)
                            return;

                        host.stopServer(servers[0].getName());
                    } catch (DevFailed e) {
                        //	Check if only moving
                        try {
                            //	Ask to confirm
                            if (JOptionPane.showConfirmDialog(parent,
                                    e.errors[0].desc + "\n" +
                                            "Do you even want to kill it (harshly) ?",
                                    "Confirm Dialog",
                                    JOptionPane.YES_NO_OPTION) != JOptionPane.OK_OPTION)
                                return;
                            host.hardKillServer(servers[0].getName());
                        } catch (DevFailed e2) {
                            ErrorPane.showErrorMessage(parent, null, e2);
                        }
                    }
                } else
                    host.startServer(parent, servers[0].getName());
                break;
        }
    }

    //======================================================
    //======================================================
    private void startPollingProfiler() {
        try {
            String[] devnames = servers[0].queryDevice();
            new PollingProfiler(parent, devnames).setVisible(true);
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(parent, null, e);
        }
    }
}
//server.stopServer(parent);
