# Multi Servers Start/Stop
## Servers Start/Stop on Host
<!--
{audience}`administrators, developers, users`
-->
Manage Servers by Host:

- All servers on a host by rigth click Menu on {doc}`Main window <main_window>` by clicking\
  on {guilabel}`Start All Servers` or {guilabel}`Stop All Servers`.
- All servers on {doc}`host window <host_window>` view by clicking on {guilabel}`Start All` or {guilabel}`Stop All`
  button.
- All servers on all hosts of a branch by using rigth click menu on selected Branch.
  ![Branch Menu](img/BranchMenu.png){.bg-transparent}
- All servers on all hosts of the control system by using
  {guilabel}`Command` menu in main window.
  ![Command Menu](img/CommandMenu.png){.bg-transparent}
- All servers on {doc}`host window <host_window>` you can also do selection of Servers with keyboard button `SHIFT` or `CTRL` and Click Selection
  ![Servers Selection Menu](img/ServersSelectionStop.png){.bg-transparent}

If you use Start/Stop All on host the servers will be started in ascending order of startup level and
stopped in descending order.

For each level a confirmation dialogbox will be popuped.

```{image} img/multi_stop.png
:class: bg-transparent
```

## Multi Servers Start/Stop

- You can start or stop several servers on serveral Hosts by `Tools` --> `Multi Servers Command`.
Choose patenrn of Server wanted and select in the list the Serves you want start or stop. You can also use shortcut like `CTRL`+`A`

```{image} img/Multi_Servers_Command.png
:class: bg-transparent
```

On {doc}`host window <host_window>`.