---
substitutions:
  image0: |-
    ```{image} img/start_event_manager.jpg
    ```
  image1: |-
    ```{image} img/dev_browser_1.jpg
    ```
  image2: |-
    ```{image} img/configure_event.png
    :class: bg-transparent
    ```
  image3: |-
    ```{image} img/dev_browser_3.jpg
    ```
  image4: |-
    ```{image} img/event_tester.jpg
    ```
---

# Event Manager

{audience}`administrators, developers`

Astor proposes to browse by server, by device or by alias.
It could be useful:

- [to see information ](#info)

- [to configure events ](#config)

- [to manage polling ](#polling)

- [to check events ](#events)

  {{ image0 }}

(info)=

## Server, Device or Alias information

> {{ image1 }}

(config)=

## Configure the events for specified attribute

> {{ image2 }}

(polling)=

## Manage polling for Server, Device or Alias

By a click on server or device menu, the {doc}`polling window <polling_window>` will be displayed.

(events)=

## Configure and Test events

By a click on attribute menu, the event tester window will be
displayed with event information.

> {{ image3 }}
>
> {{ image4 }}
