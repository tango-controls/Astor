---
substitutions:
  image0: |-
    ```{image} img/tango_versions.png
    ```
---

# Server TANGO versions

{audience}`administrators, developers`

On a host you can have an overview of TANGO version for servers

(See [TANGO releases history](http://www.tango-controls.org/about-us/))

> {{ image0 }}
