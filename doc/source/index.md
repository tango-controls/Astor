# Astor (TANGO Manager)

{audience}`developers, administrators, users`


```{toctree}
:name: Reference
:maxdepth: 2

introduction
main_window
host_window
add_host
add_server
new_server_wizard
multi_start_stop
branch_management
config
event_tester
polling_window
statistics
tango_versions
access_control
features_overview
paper
```