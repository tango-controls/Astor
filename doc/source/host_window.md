# Host Status Window
<!---
{audience}`administrators, developers, users`
-->
- When a host window has been opened, all servers controlled on this
  host are displayed.

- The color of the server define its state:

| For Servers                                               |
| --------------------------------------------------------- |
| ![green bal](img/greenball.png){.bg-transparent} Server is running                            |
| ![green bal](img/blueball.png){.bg-transparent} Server is running but not alive (Starting ?) |
| ![green bal](img/redball.png){.bg-transparent} Server is not running.                       |

- These servers are ordered by startup level.

```{image} img/host_window.jpg
```

- A popup menu will be displayed with a right click on a server to
  Start/stop/test... it.

```{image} img/host_window2.png
:class: bg-transparent
```

:::{note}
- It is possible to display "Not Controlled" servers if any.
- These servers are not taken into account to compute host state.
:::

% Image definitions
% ------------------------
